<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Zugriff verweigert',
  'actions'=>'Aktionen',
  'add_modul'=>'Modul hinzufügen',
  'back'=>'zurück',
  'delete'=>'löschen',
  'edit'=>'ändern',
  'edit_modul'=>'Modul ändern',



  'left_is_activated'=>'Links ist aktiviert?',
  'right_is_activated'=>'Rechts ist aktiviert?',
  'all_activated'=>'Links Rechts aktiviert?',
  '11all_activated'=>'alles aktiviert?',
  'all_deactivated'=>'Base aktiviert?',

  'base'=>'Base',
  'left_page'=>'Page left',
  'right_page'=>'Page right',
  'left_right_page'=>'Page left right',
  'page_head'=>'Page Head',
  'content_head'=>'Content Head',
  'content_foot'=>'Content Foot',
  'options'=>'Optionen',
  'activated'=>'aktiviert?',
  'deactivated'=>'deaktiviert',
  'new_modul'=>'neues Modul',
  'no'=>'Nein',
  'modul_name'=>'Modul Name',
  'module'=>'Modul',
  'content_head_modul'=>'Content Head Modul',
  'really_delete'=>'Dieses Modul wirklich löschen?',
  'sort'=>'Sortierung',
  'styles' => 'Stile',
  'transaction_invalid'=>'Transaktions ID ungültig',
  'to_sort'=>'sortieren',
  'yes'=>'Ja',
  'description'=>'Beschreibung',
  'info'=> '<div class="alert alert-warning" role="alert"><b>Beschreibung:</b><br>
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br></div>',
'delete_info'=>'Möchten Sie diese Moduleinstellung wirklich entfernen? <br><br>Es wird alles entgültig gelöscht.'
);

