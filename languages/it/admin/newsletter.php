<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accesso negato',
  'error_send'=>'ERRORE: Impossibile inviare il notiziario (server di posta disponibile?)',
  'html_mail'=>'Messaggio HTML',
  'newsletter'=>'Newsletter',
  'no_htmlmail'=>'Ciao! Il sistema non supporta i messaggi HTML. Hai ricevuto il seguente messaggio:',
  'profile'=>'Profilo',
  'receptionists'=>'Newsletter inviata! <br><br>Destinatario:',
  'remove'=>'Puoi eliminarti dalla lista di spedizione nel tuo profilo',
  'send'=>'Invia Newsletter',
  'send_to'=>'Invia a',
  'test'=>'Test',
  'test_newsletter'=>'Newsletter di prova',
  'title'=>'Titolo',
  'transaction_invalid'=>'ID transazione non valida',
  'user_clanmembers'=>'Membri del Clan',
  'user_newsletter'=>'Abbonati alla Newsletter',
  'user_registered'=>'Utenti registrati (Membri del Clan) ',
  'users'=>'Utente'
);

