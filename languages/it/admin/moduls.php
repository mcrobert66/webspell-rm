<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accesso vietato',
  'actions'=>'Azioni',
  'add_modul'=>'Aggiungi Modulo',
  'back'=>'Indietro',
  'delete'=>'Cancella',
  'edit'=>'Edita',
  'edit_modul'=>'Salva Modulo',
  
  
  'left_is_activated'=>'Sinistra Attivati',
  'right_is_activated'=>'Destra Attivati',
  'all_activated'=>'Sin./Des. Attivati',
  'all_deactivated'=>'Disattivati',
  
  'base'=>'Disattivati',
  'left_page'=>'Sinistra.',
  'right_page'=>'Destra.',
  'left_right_page'=>'Sinistra/Destra',
  'page_head'=>'Testata',
  'content_head'=>'Cont. Alto',
  'content_foot'=>'Cont. Basso.',
  'options'=>'Opzioni',
  
  'activated'=>'Attivi',
  'deactivated'=>'Tutti Attivi',
  'new_modul'=>'Nuovo Modulo',
  'no'=>'No',
  'modul_name'=>'Nome Modulo',
  'module'=>'Modulo',
  
  'content_head_modul'=>'Modulo Testata',
  
  'really_delete'=>'Sei sicuro di voler eliminare questo modulo?',
  'sort'=>'Ordinamento',
  'styles' => 'Stili',
  'transaction_invalid'=>'ID transazione non valida',
  'to_sort'=>'Ordina',
  'yes'=>'Si',
  'description'=>'Descrizione',
  'info'=> '<div class="alert alert-warning" role="alert"><b>Descrizione:</b><br>
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br></div>',
'delete_info'=>'Sei sicuro di voler rimuovere questa impostazione del modulo? <br><br>Tutto viene cancellato in modo Permanente.'
);

