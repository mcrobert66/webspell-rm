<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accesso negato',
  'actions'=>'Azioni',
  'add_rubric'=>'Aggiungi Rubrica',
  'back'=>'Indietro',
  'delete'=>'Cancella',
  'edit'=>'Edita',
  'edit_rubric'=>'Edita Rubrica',
  'format_incorrect'=>'Il formato dell\'icona non era corretto. Si prega di caricare solo banner in formato .gif, .jpg e .pngt.',
  'information_incomplete'=>'Mancano alcune informazioni.',
  'new_rubric'=>'Nuova Rubrica',
  'news_rubrics'=>'Rubriche Notizie',
  'picture'=>'Immagini',
  'picture_upload'=>'Upload Immagini',
  'really_delete'=>'Davvero eliminare questo Rubrica?',
  'rubric_name'=>'Nome Rubrica',
  'transaction_invalid'=>'ID transazione non valida'
);

