<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = Array(

/* do not edit above this line */

  'access_denied'=>'Accesso Negato',
  'actions'=>'Azioni',
  'add_country'=>'Aggiungi Paese',
  'back'=>'Indietro',
  'countries'=>'Paesi',
  'country'=>'Paese',
  'delete'=>'Cancella',
  'edit'=>'Edita',
  'edit_country'=>'Edita Paese',
  'favorite'=>'Favoriti',
  'fill_correctly'=>'Si prega di compilare correttamente il modulo.',
  'format_incorrect'=>'Il formato dell\'icona non era corretto. Si prega di caricare solo le icone in formato .gif.',
  'icon'=>'Icona',
  'icons'=>'Icone',
  'icon_upload'=>'Upload Icona',
  'max_18x12'=>'(Mass. 18x12)',
  'new_country'=>'Nuovo Paese',
  'no_entries'=>'Nessun Dato',
  'present_icon'=>'Icona Corrente',
  'really_delete'=>'Davvero eliminare questo paese?',
  'shorthandle'=>'Abbreviazione',
  'transaction_invalid'=>'Transazione invalida'
);

