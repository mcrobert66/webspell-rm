<?php
/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\
| _    _  ___  ___  ___  ___  ___  __    __      ___   __  __       |
|( \/\/ )(  _)(  ,)/ __)(  ,\(  _)(  )  (  )    (  ,) (  \/  )      |
| \    /  ) _) ) ,\\__ \ ) _/ ) _) )(__  )(__    )  \  )    (       |
|  \/\/  (___)(___/(___/(_)  (___)(____)(____)  (_)\_)(_/\/\_)      |
|                       ___          ___                            |
|                      |__ \        / _ \                           |
|                         ) |      | | | |                          |
|                        / /       | | | |                          |
|                       / /_   _   | |_| |                          |
|                      |____| (_)   \___/                           |
\___________________________________________________________________/
/                                                                   \
|        Copyright 2005-2018 by webspell.org / webspell.info        |
|        Copyright 2018-2019 by webspell-rm.de                      |
|                                                                   |
|        - Script runs under the GNU GENERAL PUBLIC LICENCE         |
|        - It's NOT allowed to remove this copyright-tag            |
|        - http://www.fsf.org/licensing/licenses/gpl.html           |
|                                                                   |
|               Code based on WebSPELL Clanpackage                  |
|                 (Michael Gruber - webspell.at)                    |
\___________________________________________________________________/
/                                                                   \
|                     WEBSPELL RM Version 2.0                       |
|           For Support, Mods and the Full Script visit             |
|                       webspell-rm.de                              |
\__________________________________________________________________*/

$language_array = array(

/* do not edit above this line */

    'about' => 'Chi siamo',
    'archive' => 'Archivio',
    'articles' => 'Articoli',
    'awards' => 'Premi',
    'buddys' => 'Lista Amici',
    'calendar' => 'Calendario',
    'cash_box' => 'Cassa portavalori',
    'challenge' => 'Challenges',
    'clanwars' => 'Guerre del clan',
    'clanwars_details' => 'Guerra del clan contro',
    'contact' => 'Contatti',
    'demo' => 'Demo',
    'demos' => 'Demo',
    'faq'=>'FAQ',
    'files' => 'File',
    'forum' => 'Forum',
    'gallery' => 'Galleria',
    'guestbook' => 'Guestbook',
    'history' => 'Storia',
    'imprint' => 'Imprint',
    'joinus' => 'Entra con noi',
    'links' => 'Link',
    'linkus' => 'Lincaci',
    'login' => 'Login',
    'loginoverview' => 'Panoramica d\'accesso',
    'lostpassword' => 'Richiesta cambio Password',
    'members' => 'Memberi',
    'messenger' => 'Messaggi',
    'myprofile' => 'Edita Profilo',
    'news' => 'Home Page',
    'newsletter' => 'Newsletter',
    'partners' => 'Partner',
    'polls' => 'Sondaggi',
    'profile' => 'Profilo di',
    'register' => 'Registrazione',
    'registered_users' => 'Utenti Registrati',
    'search' => 'Ricerca',
    'server' => 'Server',
    'shoutbox' => 'Shoutbox',
    'sponsors' => 'Sponsor',
    'squads' => 'Squadre',
    'start' => 'Pagina principale',
    'stats' => 'Statistiche',
    'usergallery' => 'Gallerie Utenti',
    'versus' => 'versus',
	'polls' => 'Sondaggi',
    'whoisonline' => 'Chi c\'è On-Line'
);

